﻿#pragma once

#include <SDL.h>

#include "../Kinect/KinectHandle.hpp"

class App
{
public:
    App() = default;
    App(App const&) = delete;
    App(App&&) noexcept = delete;
    ~App() noexcept;

    //------------------------------------------------------------------------------------------------------------------

    bool init(int screen_width = 512, int screen_height = 424);
    void run();
    
    //------------------------------------------------------------------------------------------------------------------

    App& operator=(App const&) = delete;
    App& operator=(App&&) noexcept = delete;

private:
    int screen_width_{512};
    int screen_height_{424};
    bool is_running_{false};

    SDL_Window* window_{nullptr};
    SDL_Texture* texture_{nullptr};
    SDL_Renderer* renderer_{nullptr};

    kinect::KinectHandle kinect_handle_{};
    
    //------------------------------------------------------------------------------------------------------------------

    void draw_pixel_buffer(uint32_t* pixel_buffer) const;
}; 
