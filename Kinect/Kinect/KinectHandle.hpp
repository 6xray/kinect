﻿#pragma once

#include <thread>
#include <unordered_map>

#include <Kinect.h>

#include "BaseFrameHandle.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    class KinectHandle
    {
    public:
        KinectHandle() = default;
        KinectHandle(KinectHandle const&) = delete;
        KinectHandle(KinectHandle&&) noexcept = delete;
        ~KinectHandle() noexcept;

        // -------------------------------------------------------------------------------------------------------------

        bool start(bool should_auto_update = true);
        void update() const;
        void stop();

        template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool> = true>
        bool add_frame_handle();
        template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool> = true>
        [[nodiscard]] FrameHandle* get_frame_handle() const;
        template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool> = true>
        bool remove_frame_handle();

        bool add_all_handles();
        void remove_all_handles();

        // -------------------------------------------------------------------------------------------------------------

        KinectHandle& operator=(KinectHandle const&) = delete;
        KinectHandle& operator=(KinectHandle&&) = delete;

    private:
        bool is_running_{false};

        float update_rate_{60};
        std::thread update_thread_{};

        IKinectSensor* kinect_sensor_{nullptr};
        ICoordinateMapper* coordinate_mapper_{nullptr};

        std::unordered_map<uint64_t, std::shared_ptr<BaseFrameHandle>> frame_handles_{};

        // -------------------------------------------------------------------------------------------------------------

        bool init_kinect_sensor();
        bool init_coordinate_mapper();

        void threaded_update() const;
    };

    // -----------------------------------------------------------------------------------------------------------------

    template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool>>
    bool KinectHandle::add_frame_handle()
    {
        uint64_t const hash_code = typeid(FrameHandle).hash_code();

        if (frame_handles_.find(hash_code) != frame_handles_.end())
            return false;

        frame_handles_[hash_code] = std::make_shared<FrameHandle>();

        return true;
    }

    template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool>>
    FrameHandle* KinectHandle::get_frame_handle() const
    {
        uint64_t const hash_code = typeid(FrameHandle).hash_code();

        if (frame_handles_.find(hash_code) == frame_handles_.end())
            return nullptr;

        return dynamic_cast<FrameHandle*>(frame_handles_.at(hash_code).get());
    }

    template <typename FrameHandle, std::enable_if_t<std::is_base_of_v<BaseFrameHandle, FrameHandle>, bool>>
    bool KinectHandle::remove_frame_handle()
    {
        uint64_t const hash_code = typeid(FrameHandle).hash_code();

        if (frame_handles_.find(hash_code) == frame_handles_.end())
            return false;

        frame_handles_.erase(hash_code);

        return true;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

#include "BodyFrameHandle.hpp"
#include "BodyIndexFrameHandle.hpp"
#include "ColorFrameHandle.hpp"
#include "DepthFrameHandle.hpp"
#include "InfraredFrameHandle.hpp"
#include "LongExposureInfraredFrameHandle.hpp"

namespace kinect
{
    inline KinectHandle::~KinectHandle() noexcept
    {
        stop();
    }

    // -----------------------------------------------------------------------------------------------------------------

    inline bool KinectHandle::start(bool const should_auto_update)
    {
        if (is_running_)
            return true;

        if (!init_kinect_sensor() || !init_coordinate_mapper())
            return false;

        is_running_ = true;

        for (auto& [key, value] : frame_handles_)
        {
            if (value)
                value->start(kinect_sensor_);
        }

        if (should_auto_update)
        {
            update_thread_ = std::thread(&KinectHandle::threaded_update, this);
        }

        return true;
    }

    inline void KinectHandle::update() const
    {
        if (!is_running_)
            return;

        for (auto& [key, value] : frame_handles_)
        {
            if (value)
                value->update();
        }
    }

    inline void KinectHandle::stop()
    {
        if (!is_running_)
            return;

        is_running_ = false;

        update_thread_.join();

        kinect_sensor_->Release();
        coordinate_mapper_->Release();

        kinect_sensor_ = nullptr;
        coordinate_mapper_ = nullptr;

        for (auto [fst, snd] : frame_handles_)
        {
            if (snd)
                snd->stop();
        }
    }

    inline bool KinectHandle::add_all_handles()
    {
        return add_frame_handle<BodyFrameHandle>() && add_frame_handle<ColorFrameHandle>() &&
            add_frame_handle<DepthFrameHandle>() && add_frame_handle<InfraredFrameHandle>() &&
            add_frame_handle<BodyIndexFrameHandle>() && add_frame_handle<LongExposureInfraredFrameHandle>();
    }

    inline void KinectHandle::remove_all_handles()
    {
        frame_handles_.clear();
    }

    // -----------------------------------------------------------------------------------------------------------------

    inline bool KinectHandle::init_kinect_sensor()
    {
        if (FAILED(GetDefaultKinectSensor(&kinect_sensor_)))
            return false;

        if (FAILED(kinect_sensor_->Open()))
            return false;

        BOOLEAN check{0};

        kinect_sensor_->get_IsAvailable(&check);

        return check;
    }

    inline bool KinectHandle::init_coordinate_mapper()
    {
        if (!kinect_sensor_)
            return false;

        return SUCCEEDED(kinect_sensor_->get_CoordinateMapper(&coordinate_mapper_));
    }

    inline void KinectHandle::threaded_update() const
    {
        auto const frame_duration = std::chrono::milliseconds(1000 / static_cast<int>(update_rate_));
        auto next_frame_time = std::chrono::steady_clock::now();

        while (is_running_)
        {
            next_frame_time += frame_duration;

            update();

            std::this_thread::sleep_until(next_frame_time);
        }
    }
}
