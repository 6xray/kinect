﻿#pragma once

#include <Kinect.h>
#include <mutex>

#include "BaseFrameHandle.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    class BodyIndexFrameHandle final : public BaseFrameHandle
    {
    public:
        BodyIndexFrameHandle() = default;
        BodyIndexFrameHandle(BodyIndexFrameHandle const&) = delete;
        BodyIndexFrameHandle(BodyIndexFrameHandle&&) noexcept = delete;
        ~BodyIndexFrameHandle() noexcept override;

        // -------------------------------------------------------------------------------------------------------------

        void start(IKinectSensor* kinect_sensor) override;
        void update() override;
        void stop() override;

        [[nodiscard]] uint8_t* get_frame_buffer();
        
        // -------------------------------------------------------------------------------------------------------------

        BodyIndexFrameHandle& operator=(BodyIndexFrameHandle const&) = delete;
        BodyIndexFrameHandle& operator=(BodyIndexFrameHandle&&) noexcept = delete;

    private:
        std::mutex mutex_;
        int frame_width_{0};
        int frame_height_{0};
        
        IBodyIndexFrame* frame_{nullptr};
        IBodyIndexFrameSource* frame_source_{nullptr};
        IBodyIndexFrameReader* frame_reader_{nullptr};
        std::unique_ptr<uint8_t> frame_buffer_{nullptr};
    };
}

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
	inline BodyIndexFrameHandle::~BodyIndexFrameHandle() noexcept
	{
		stop();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline void BodyIndexFrameHandle::start(IKinectSensor* kinect_sensor)
	{
		if (!kinect_sensor)
			throw std::runtime_error("kinect_sensor is not valid");

		HRESULT check = kinect_sensor->get_BodyIndexFrameSource(&frame_source_);

		if (FAILED(check) || !frame_source_)
			throw std::runtime_error("Failed to get BodyFrameSource");

		IFrameDescription* frame_description{ nullptr };

		check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
		{
			safe_release(frame_source_);
			throw std::runtime_error("Failed to get BodyIndexFrameReader");
		}

		frame_buffer_.reset(new uint8_t[frame_width_ * frame_height_]);
	}

	inline void BodyIndexFrameHandle::update()
	{
		if (!frame_source_ || !frame_reader_)
			return;

		IFrameDescription* frame_description{ nullptr };

		HRESULT check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
			return;

		check = frame_reader_->AcquireLatestFrame(&frame_);

		if (FAILED(check) || !frame_)
			return;

		int const size = frame_width_ * frame_height_;
		std::unique_ptr<uint8_t> const buffer{ new uint8_t[size] };

		check = frame_->CopyFrameDataToArray(size, buffer.get());

		safe_release(frame_);

		if (FAILED(check))
			return;

		std::lock_guard<std::mutex> lock(mutex_);

		for (int i = 0; i < size; i++)
		{
			frame_buffer_.get()[i] = buffer.get()[i];
		}
	}

	inline void BodyIndexFrameHandle::stop()
	{
		{
			std::lock_guard<std::mutex> lock{ mutex_ };
			frame_buffer_.reset(nullptr);
		}

		safe_release(frame_);
		safe_release(frame_source_);
		safe_release(frame_reader_);
	}

	inline uint8_t* BodyIndexFrameHandle::get_frame_buffer()
	{
		std::lock_guard<std::mutex> lock(mutex_);

		return frame_buffer_.get();
	}
}