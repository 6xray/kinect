﻿#pragma once

#include <Kinect.h>
#include <mutex>

#include "BaseFrameHandle.hpp"

namespace kinect
{
    class DepthFrameHandle final : public BaseFrameHandle
    {
    public:
        DepthFrameHandle() = default;
        DepthFrameHandle(DepthFrameHandle const&) = delete;
        DepthFrameHandle(DepthFrameHandle&&) noexcept = delete;
        ~DepthFrameHandle() noexcept override;

        // -------------------------------------------------------------------------------------------------------------

        void start(IKinectSensor* kinect_sensor) override;
        void update() override;
        void stop() override;

        void set_white_as_forward(bool value);
        
        [[nodiscard]] uint32_t* get_frame_buffer();

        // -------------------------------------------------------------------------------------------------------------

        DepthFrameHandle& operator=(DepthFrameHandle const&) = delete;
        DepthFrameHandle& operator=(DepthFrameHandle&&) noexcept = delete;
    
    private:
        std::mutex mutex_;

        int frame_width_{0};
        int frame_height_{0};
        
        bool is_white_forward_{true};
        uint16_t min_depth_{0};
        uint16_t max_depth_{0};
        
        IDepthFrame* frame_{nullptr};
        IDepthFrameSource* frame_source_{nullptr};
        IDepthFrameReader* frame_reader_{nullptr};
        std::unique_ptr<uint32_t> frame_buffer_{nullptr};

        // -------------------------------------------------------------------------------------------------------------

        uint32_t smooth_depth_pixel(uint16_t source_pixel) const;
    };
}

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
	inline DepthFrameHandle::~DepthFrameHandle() noexcept
	{
		stop();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline void DepthFrameHandle::start(IKinectSensor* kinect_sensor)
	{
		if (!kinect_sensor)
			throw std::runtime_error("kinect_sensor is not valid");

		HRESULT check = kinect_sensor->get_DepthFrameSource(&frame_source_);

		if (FAILED(check) || !frame_source_)
			throw std::runtime_error("Failed to get DepthFrameSource");

		IFrameDescription* frame_description{ nullptr };

		check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
		{
			safe_release(frame_source_);
			throw std::runtime_error("Failed to get DepthFrameReader");
		}

		frame_buffer_.reset(new uint32_t[frame_width_ * frame_height_]);
	}

	inline void DepthFrameHandle::update()
	{
		if (!frame_source_ || !frame_reader_)
			return;

		IFrameDescription* frame_description{ nullptr };

		HRESULT check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
			return;

		check = frame_reader_->AcquireLatestFrame(&frame_);

		if (FAILED(check) || !frame_)
			return;

		int const size = frame_width_ * frame_height_;
		std::unique_ptr<uint16_t> const buffer{ new uint16_t[size] };

		check = frame_->CopyFrameDataToArray(size, buffer.get());

		frame_->get_DepthMinReliableDistance(&min_depth_);
		frame_->get_DepthMaxReliableDistance(&max_depth_);

		safe_release(frame_);

		if (FAILED(check))
			return;

		std::lock_guard<std::mutex> lock(mutex_);

		uint32_t* frame_ptr = frame_buffer_.get();
		uint16_t* buffer_ptr = buffer.get();

		for (int i = 0; i < size; i++)
		{
			frame_ptr[i] = smooth_depth_pixel(buffer_ptr[i]);
		}
	}

	inline void DepthFrameHandle::stop()
	{
		{
			std::lock_guard<std::mutex> lock{ mutex_ };
			frame_buffer_.reset(nullptr);
		}

		safe_release(frame_);
		safe_release(frame_source_);
		safe_release(frame_reader_);
	}

	inline void DepthFrameHandle::set_white_as_forward(bool const value)
	{
		is_white_forward_ = value;
	}

	inline uint32_t* DepthFrameHandle::get_frame_buffer()
	{
		std::lock_guard<std::mutex> lock{ mutex_ };

		return frame_buffer_.get();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline uint32_t DepthFrameHandle::smooth_depth_pixel(uint16_t const source_pixel) const
	{
		uint32_t output{ 0 };
		auto const output_ptr = reinterpret_cast<uint8_t*>(&output);

		uint16_t const max_depth_to_byte = max_depth_ / 256;
		uint16_t const intensity = source_pixel >= min_depth_ && source_pixel <= max_depth_
			? is_white_forward_
			? 255 - source_pixel / max_depth_to_byte
			: source_pixel / max_depth_to_byte
			: 0;

		output_ptr[0] = intensity;
		output_ptr[1] = intensity;
		output_ptr[2] = intensity;
		output_ptr[3] = 0xff;

		return output;
	}
}