﻿#pragma once

#include <Kinect.h>
#include <tuple>
#include <vector>

#include "BaseFrameHandle.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    struct BodyData
    {
        int id{-1};

        bool is_tracked{false};
        bool is_left_hand_open{false};
        bool is_right_hand_open{false};

        std::tuple<float, float, float> body_pos{0, 0, 0};
        std::tuple<float, float, float> head_pos{0, 0, 0};
        std::tuple<float, float, float> left_hand_pos{0, 0, 0};
        std::tuple<float, float, float> right_hand_pos{0, 0, 0};
        std::tuple<float, float, float, float> body_rot{0, 0, 0, 1};
    };

    // -----------------------------------------------------------------------------------------------------------------

    class BodyFrameHandle final : public BaseFrameHandle
    {
    public:
        BodyFrameHandle() = default;
        BodyFrameHandle(BodyFrameHandle const&) = delete;
        BodyFrameHandle(BodyFrameHandle&&) noexcept = delete;
        ~BodyFrameHandle() noexcept override;

        // -------------------------------------------------------------------------------------------------------------

        void start(IKinectSensor* kinect_sensor) override;
        void update() override;
        void stop() override;

        [[nodiscard]] std::vector<BodyData> get_bodies() const noexcept;

        // -------------------------------------------------------------------------------------------------------------

        BodyFrameHandle& operator=(BodyFrameHandle const&) = delete;
        BodyFrameHandle& operator=(BodyFrameHandle&&) noexcept = delete;

    private:
        int body_count_{0};
        std::vector<IBody*> raw_bodies_;
        std::vector<BodyData> bodies_;

        IBodyFrame* frame_{nullptr};
        IBodyFrameSource* frame_source_{nullptr};
        IBodyFrameReader* frame_reader_{nullptr};

        // -------------------------------------------------------------------------------------------------------------

        BodyData generate_body_data(IBody* body);
    };
}

// ---------------------------------------------------------------------------------------------------------------------

#include <array>
#include <stdexcept>

namespace kinect
{
    inline BodyFrameHandle::~BodyFrameHandle() noexcept
    {
        stop();
    }

    // -----------------------------------------------------------------------------------------------------------------

    inline void BodyFrameHandle::start(IKinectSensor* kinect_sensor)
    {
        if (!kinect_sensor)
            throw std::runtime_error("kinect_sensor is not valid");

        HRESULT check = kinect_sensor->get_BodyFrameSource(&frame_source_);

        if (FAILED(check) || !frame_source_)
            throw std::runtime_error("Failed to get ColorFrameSource");

        check = frame_source_->OpenReader(&frame_reader_);

        if (FAILED(check) || !frame_reader_)
        {
            safe_release(frame_source_);
            throw std::runtime_error("Failed to get ColorFrameReader");
        }
    }

    inline void BodyFrameHandle::update()
    {
        if (!frame_reader_ || !frame_source_)
            return;

        frame_source_->get_BodyCount(&body_count_);
        raw_bodies_.resize(body_count_);

        if (FAILED(frame_reader_->AcquireLatestFrame(&frame_)) || !frame_)
            return;

        frame_->GetAndRefreshBodyData(body_count_, raw_bodies_.data());

        safe_release(frame_);

        bodies_.clear();

        for (auto body : raw_bodies_)
        {
            BodyData const body_data = generate_body_data(body);

            bodies_.push_back(body_data);
        }
    }

    inline void BodyFrameHandle::stop()
    {
        safe_release(frame_);
        safe_release(frame_reader_);
        safe_release(frame_source_);
    }

    inline std::vector<BodyData> BodyFrameHandle::get_bodies() const noexcept
    {
        return bodies_;
    }

    // -----------------------------------------------------------------------------------------------------------------

    inline BodyData BodyFrameHandle::generate_body_data(IBody* const body)
    {
        BodyData body_data;

        if (!body)
            return body_data;

        BOOLEAN is_tracked{0};
        uint64_t tracking_id{0};
        HandState left_hand_state{HandState_NotTracked};
        HandState right_hand_state{HandState_NotTracked};

        body->get_IsTracked(&is_tracked);
        body->get_TrackingId(&tracking_id);
        body->get_HandLeftState(&left_hand_state);
        body->get_HandRightState(&right_hand_state);

        std::array<Joint, 25> joints;
        std::array<JointOrientation, 25> joint_orientations;
        size_t const joint_count = joints.size();

        body->GetJoints(joint_count, joints.data());
        body->GetJointOrientations(joint_count, joint_orientations.data());

        for (size_t i = 0; i < joint_count; i++)
        {
            if (auto const [JointType, Position, TrackingState] = joints[i]; TrackingState == TrackingState_Tracked)
            {
                switch (JointType)
                {
                case JointType_Head:
                    body_data.head_pos = std::make_tuple(Position.X, Position.Y, Position.Z);
                    break;

                case JointType_SpineBase:
                    body_data.body_pos = std::make_tuple(Position.X, Position.Y, Position.Z);
                    break;

                case JointType_HandLeft:
                    body_data.left_hand_pos = std::make_tuple(Position.X, Position.Y, Position.Z);
                    break;

                case JointType_HandRight:
                    body_data.right_hand_pos = std::make_tuple(Position.X, Position.Y, Position.Z);
                    break;

                default:
                    break;
                }
            }

            switch (auto const [JointType, Orientation] = joint_orientations[i]; JointType)
            {
            case JointType_SpineBase:
                body_data.body_rot = std::make_tuple(Orientation.x, Orientation.y, Orientation.z, Orientation.w);
                break;

            default:
                break;
            }
        }

        body_data.id = tracking_id;
        body_data.is_tracked = is_tracked;
        body_data.is_left_hand_open = left_hand_state == HandState_Lasso ||
            left_hand_state == HandState_Open;
        body_data.is_right_hand_open = right_hand_state == HandState_Lasso ||
            right_hand_state == HandState_Open || right_hand_state == HandState_Unknown;

        return body_data;
    }
}
