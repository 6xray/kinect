#include "App/App.h"
#undef main

int main()
{
    App app{};

    if (app.init())
    {
        app.run();
    }
    else
    {
        printf_s("[Error] Failed to start. Check previous errors to find the issue.\n");
    }
}
