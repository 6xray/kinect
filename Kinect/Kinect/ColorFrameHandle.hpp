﻿#pragma once

#include <Kinect.h>
#include <mutex>

#include "BaseFrameHandle.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    class ColorFrameHandle final : public BaseFrameHandle
    {
    public:
        ColorFrameHandle() = default;
        ColorFrameHandle(ColorFrameHandle const&) = delete;
        ColorFrameHandle(ColorFrameHandle&&) noexcept = delete;
        ~ColorFrameHandle() noexcept override;

        // -------------------------------------------------------------------------------------------------------------

        void start(IKinectSensor* kinect_sensor) override;
        void update() override;
        void stop() override;
        
        [[nodiscard]] uint32_t* get_framer_buffer();

        // -------------------------------------------------------------------------------------------------------------

        ColorFrameHandle& operator=(ColorFrameHandle const&) = delete;
        ColorFrameHandle& operator=(ColorFrameHandle&&) noexcept = delete;

    private:
        std::mutex mutex_;
        int frame_width_{0};
        int frame_height_{0};
        
        IColorFrame* frame_{nullptr};
        IColorFrameSource* frame_source_{nullptr};
        IColorFrameReader* frame_reader_{nullptr};
        std::unique_ptr<uint32_t> frame_buffer_{nullptr};
    };
}

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
	inline ColorFrameHandle::~ColorFrameHandle() noexcept
	{
		stop();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline void ColorFrameHandle::start(IKinectSensor* kinect_sensor)
	{
		if (!kinect_sensor)
			throw std::runtime_error("kinect_sensor is not valid");

		HRESULT check = kinect_sensor->get_ColorFrameSource(&frame_source_);

		if (FAILED(check) || !frame_source_)
			throw std::runtime_error("Failed to get ColorFrameSource");

		IFrameDescription* frame_description{ nullptr };

		check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
		{
			safe_release(frame_source_);
			throw std::runtime_error("Failed to get ColorFrameReader");
		}

		frame_buffer_.reset(new uint32_t[frame_width_ * frame_height_]);
	}

	inline void ColorFrameHandle::update()
	{
		if (!frame_source_ || !frame_reader_)
			return;

		IFrameDescription* frame_description{ nullptr };

		HRESULT check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
			return;

		check = frame_reader_->AcquireLatestFrame(&frame_);

		if (FAILED(check) || !frame_)
			return;

		int const size = frame_width_ * frame_height_;
		std::unique_ptr<uint32_t> const buffer{ new uint32_t[size] };

		check = frame_->CopyConvertedFrameDataToArray(size * 4, reinterpret_cast<BYTE*>(buffer.get()),
			ColorImageFormat_Bgra);

		safe_release(frame_);

		if (FAILED(check))
			return;

		std::lock_guard<std::mutex> lock(mutex_);

		memcpy(frame_buffer_.get(), buffer.get(), size);
	}

	inline void ColorFrameHandle::stop()
	{
		{
			std::lock_guard<std::mutex> lock{ mutex_ };
			frame_buffer_.reset(nullptr);
		}

		safe_release(frame_);
		safe_release(frame_source_);
		safe_release(frame_reader_);
	}

	inline uint32_t* ColorFrameHandle::get_framer_buffer()
	{
		std::lock_guard<std::mutex> lock{ mutex_ };

		return frame_buffer_.get();
	}
}