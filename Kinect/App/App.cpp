﻿#include "App.h"

#include "../Kinect/DepthFrameHandle.hpp"

App::~App() noexcept
{
    SDL_DestroyTexture(texture_);
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

//----------------------------------------------------------------------------------------------------------------------

bool App::init(int const screen_width, int const screen_height)
{
    if (screen_width < 0 || screen_height < 0)
        return false;

    screen_width_ = screen_width;
    screen_height_ = screen_height;

    SDL_Init(SDL_INIT_VIDEO);

    window_ = SDL_CreateWindow("Kinect SDK", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen_width_,
                               screen_height_, 0);

    if (!window_)
        return false;

    renderer_ = SDL_CreateRenderer(window_, -2, SDL_RENDERER_ACCELERATED);

    if (!renderer_)
        return false;

    texture_ = SDL_CreateTexture(renderer_, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, screen_width_,
                                 screen_height_);

    if (!texture_)
        return false;

    is_running_ = kinect_handle_.add_all_handles() && kinect_handle_.start();

    return is_running_;
}

void App::run()
{
    while (is_running_)
    {
        uint32_t const frame_start = SDL_GetTicks();

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                is_running_ = false;
                break;

            case SDL_KEYDOWN:
                if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
                    is_running_ = false;
                break;

            default:
                break;
            }
        }

        auto handle = kinect_handle_.get_frame_handle<kinect::DepthFrameHandle>();

        draw_pixel_buffer(handle ? handle->get_frame_buffer() : nullptr);

        uint32_t const frame_time = SDL_GetTicks() - frame_start;

        if (1000 / 60 > frame_time)
        {
            SDL_Delay(1000 / 60 - frame_time);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

void App::draw_pixel_buffer(uint32_t* pixel_buffer) const
{
    if (pixel_buffer == nullptr)
        return;

    void* pixels{nullptr};
    int pitch{};

    SDL_LockTexture(texture_, nullptr, &pixels, &pitch);

    if (pitch != screen_width_ * 4)
    {
        uint32_t const* src = pixel_buffer;
        auto dest = static_cast<char*>(pixels);

        for (int y = 0; y < screen_height_; y++)
        {
            memcpy(dest, src, screen_width_ * 4);

            src += screen_width_;
            dest += pitch;
        }
    }
    else
    {
        memcpy(pixels, pixel_buffer, screen_width_ * screen_height_ * 4);
    }

    SDL_UnlockTexture(texture_);

    SDL_RenderCopy(renderer_, texture_, nullptr, nullptr);
    SDL_RenderPresent(renderer_);
}
