﻿#pragma once

#include <Kinect.h>

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    class BaseFrameHandle
    {
    public:
        BaseFrameHandle() = default;
        BaseFrameHandle(BaseFrameHandle const&) = default;
        BaseFrameHandle(BaseFrameHandle&&) noexcept = default;
        virtual ~BaseFrameHandle() = default;

        // -------------------------------------------------------------------------------------------------------------

        virtual void start(IKinectSensor* kinect_sensor) = 0;
        virtual void update() = 0;
        virtual void stop() = 0;

    protected:
        template <typename T>
        void safe_release(T& pointer)
        {
            if (pointer)
            {
                pointer->Release();
                pointer = nullptr;
            }
        }
    };
}
