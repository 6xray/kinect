﻿#pragma once

#include <Kinect.h>
#include <mutex>

#include "BaseFrameHandle.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace kinect
{
    class InfraredFrameHandle final : public BaseFrameHandle
    {
    public:
        InfraredFrameHandle() = default;
        InfraredFrameHandle(InfraredFrameHandle const&) = delete;
        InfraredFrameHandle(InfraredFrameHandle&&) noexcept = delete;
        ~InfraredFrameHandle() noexcept override;

        // -------------------------------------------------------------------------------------------------------------

        void start(IKinectSensor* kinect_sensor) override;
        void update() override;
        void stop() override;

        [[nodiscard]] uint32_t* get_frame_buffer();

        // -------------------------------------------------------------------------------------------------------------

        InfraredFrameHandle& operator=(InfraredFrameHandle const&) = delete;
        InfraredFrameHandle& operator=(InfraredFrameHandle&&) noexcept = delete;
    
    private:
        std::mutex mutex_;
        int frame_width_{0};
        int frame_height_{0};
        
        IInfraredFrame* frame_{nullptr};
        IInfraredFrameSource* frame_source_{nullptr};
        IInfraredFrameReader* frame_reader_{nullptr};
        std::unique_ptr<uint32_t> frame_buffer_{nullptr};

        // -------------------------------------------------------------------------------------------------------------

        uint32_t smooth_infrared_pixel(uint16_t source_pixel) const;
    };
}

// ---------------------------------------------------------------------------------------------------------------------

#include <stdexcept>

#undef max

namespace kinect
{
	inline InfraredFrameHandle::~InfraredFrameHandle() noexcept
	{
		stop();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline void InfraredFrameHandle::start(IKinectSensor* kinect_sensor)
	{
		if (!kinect_sensor)
			throw std::runtime_error("kinect_sensor is not valid");

		HRESULT check = kinect_sensor->get_InfraredFrameSource(&frame_source_);

		if (FAILED(check) || !frame_source_)
			throw std::runtime_error("Failed to get InfraredFrameSource");

		IFrameDescription* frame_description{ nullptr };

		check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
		{
			safe_release(frame_source_);
			throw std::runtime_error("Failed to get InfraredFrameReader");
		}

		frame_buffer_.reset(new uint32_t[frame_width_ * frame_height_]);
	}

	inline void InfraredFrameHandle::update()
	{
		if (!frame_source_ || !frame_reader_)
			return;

		IFrameDescription* frame_description{ nullptr };

		HRESULT check = frame_source_->get_FrameDescription(&frame_description);

		if (FAILED(check) || !frame_description)
			return;

		frame_description->get_Width(&frame_width_);
		frame_description->get_Height(&frame_height_);

		safe_release(frame_description);

		check = frame_source_->OpenReader(&frame_reader_);

		if (FAILED(check) || !frame_reader_)
			return;

		check = frame_reader_->AcquireLatestFrame(&frame_);

		if (FAILED(check) || !frame_)
			return;

		int const size = frame_width_ * frame_height_;
		std::unique_ptr<uint16_t> const buffer{ new uint16_t[size] };

		check = frame_->CopyFrameDataToArray(size, buffer.get());

		safe_release(frame_);

		if (FAILED(check))
			return;

		std::lock_guard<std::mutex> lock(mutex_);

		uint32_t* frame_ptr = frame_buffer_.get();
		uint16_t* buffer_ptr = buffer.get();

		for (int i = 0; i < size; i++)
		{
			frame_ptr[i] = smooth_infrared_pixel(buffer_ptr[i]);
		}
	}

	inline void InfraredFrameHandle::stop()
	{
		{
			std::lock_guard<std::mutex> lock{ mutex_ };
			frame_buffer_.reset(nullptr);
		}

		safe_release(frame_);
		safe_release(frame_source_);
		safe_release(frame_reader_);
	}

	inline uint32_t* InfraredFrameHandle::get_frame_buffer()
	{
		std::lock_guard<std::mutex> lock(mutex_);

		return frame_buffer_.get();
	}

	// -----------------------------------------------------------------------------------------------------------------

	inline uint32_t InfraredFrameHandle::smooth_infrared_pixel(uint16_t const source_pixel) const
	{
		constexpr float source_max_value = std::numeric_limits<uint16_t>::max();
		float const source_scale = 1.f;
		float const max_value = 1.0f;
		float const min_value = 0.01f;

		float const smooth_pixel = static_cast<float>(source_pixel) / source_max_value * source_scale * (1.0f - min_value) +
			min_value;

		float const value = max_value > smooth_pixel ? smooth_pixel : max_value;

		return 0x00010101 * static_cast<uint32_t>(0xff * sqrt(value)) | 0xff000000;
	}
}
